# Counter App

Counter App is a simple Flutter application that allows users to increment and decrement a counter value. It also includes additional features such as displaying a dialog box and navigating to a new screen based on certain conditions.

## Features

- Display a counter value and two buttons: "Increment" and "Decrement"
- Increment the counter value by 1 when the "Increment" button is pressed
- Decrement the counter value by 1 when the "Decrement" button is pressed
- Display a dialog box with the message "Counter value is 5!" when the counter reaches 5
- Navigate to a new screen called "Second Screen" and display the message "Congratulations! You reached 10!" when the counter reaches 10
- Responsive button sizes using `Expanded` and `Flexible` widgets

## Screenshots

<img src="https://gitlab.com/hasan082/live-test-module-7/-/raw/main/screenshot.png" alt="" width="270">

<img src="https://gitlab.com/hasan082/live-test-module-7/-/raw/main/screenshot2.png" alt="" width="270">

<img src="https://gitlab.com/hasan082/live-test-module-7/-/raw/main/screenshot3.png" alt="" width="270">



## Getting Started

To run the Counter App project on your local machine, follow these steps:

1. Ensure that you have Flutter SDK installed on your machine. For installation instructions, refer to the [Flutter documentation](https://flutter.dev/docs/get-started/install).

2. Clone this repository or download the project files.

3. Open the project in your preferred IDE (e.g., Visual Studio Code, Android Studio).

4. Install the required dependencies by running the following command in the project directory:

   ```bash
   flutter pub get
   ```

5. Connect your device or start an emulator.

6. Run the app using the following command:

   ```bash
   flutter run
   ```

7. The Counter App should now be running on your device or emulator.

## Dependencies

The Counter App project relies on the following Flutter packages:

- `flutter/material.dart`: The foundation for Flutter's Material Design widgets and styles.

## Contributing

Contributions to the Counter App project are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

## License

The Counter App project is open-source and released under the [MIT License](https://opensource.org/licenses/MIT).




