import 'package:flutter/material.dart';

import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  final int reachVal;

  const SecondScreen({Key? key, required this.reachVal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, true);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Second Screen'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Congratulations! You reached',
                  style: TextStyle(fontSize: 24),
                ),
                const SizedBox(height: 20,),
                Container(
                  alignment: Alignment.center,
                  width: 80,
                  height: 80,
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(40)),
                  child: Text(
                    '$reachVal',
                    style: const TextStyle(fontSize: 26, color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
